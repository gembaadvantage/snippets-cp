import "@material/typography/dist/mdc.typography.css";
import "mono-icons/iconfont/icons.css";
import { createRoot } from "react-dom/client";
import App from "./App";
import "./index.css";
import "./theme.css";

const container = document.getElementById("root");
createRoot(container).render(<App tab="home" />);
