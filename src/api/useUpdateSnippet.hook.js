// Hook to update a snippet by id

import { useCallback, useState } from "react";
import axios from "axios";

export function useUpdateSnippet(baseUrl = "/api") {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  function update({ snippetId, body, onUpdated = () => {} } = {}) {
    const url = baseUrl + "/snippets/" + snippetId;
    setLoading(true);
    setData(null);
    setError(null);
    axios
      .put(url, body)
      .then((res) => {
        setData(res.data);
        onUpdated();
      })
      .catch(setError)
      .finally(() => {
        setLoading(false);
      });
  }

  return [useCallback(update, [baseUrl]), { loading, data, error }];
}
